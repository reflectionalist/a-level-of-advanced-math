% Test for IAL Mathematics Tiral Course
% July 22, 2016

# Questions

1.  Through the lens of calculus, you can view the world in two different ways.  What are they?

    \vspace{1cm}

2.  Differentiation and integration are inverse operation of each other.  In mathematical lingo, that is,
    \begin{align*}
     \int d(I) & =  \\
     d\left(\int D\right) & =
    \end{align*}

3.  What is the analytic interpretation of the derivative at a point on a curve?

    \vspace{2cm}

4.  What is the analytic interpretation of a definite integral like $\int_a^b y\ dx$?

    \vspace{2cm}

\pagebreak

3.  How are the formulas about a circle, disc and sphere related to each other?

    \vspace{5cm}

4.  Calculate the derivative of $y = x(x - 1)(x - 2)$ at $x = \frac{1}{2}$.  (_adapted from C12 exam, May 2016_)

    \vspace{6cm}

5.  Calculate the definite integral of $y = 6x - 3 - \frac{1}{2x^2}$ from $x = 1$ to $x = 4$.  (_adapted from C12 exam, January 2016_)

    \vspace{6cm}

# Formulas

-   Circle circumference: $C = 2\pi r$
-   Disc area: $A = \pi r^2$
-   Sphere volume: $V = \frac{4}{3}\pi r^3$
-   Sphere surface area: $S = 4\pi r^2$
