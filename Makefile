JNC = jupyter nbconvert
MDC = pandoc
TEX = xelatex
INF = draft.ipynb
IFN = $(basename ${INF})
OFN = ${IFN}
NOS =
PRO = $(if $(and ${NOS}, ${TOC}), \
           --number-sections --table-of-contents, \
      $(if ${NOS}, \
           --number-sections, \
      $(if ${TOC}, \
           --table-of-contents)))
CSL = styles/chicago-author-date-in-brackets.csl
PSO = $(if $(INC), \
	   --incremental)
#TDS = report slides

.ONESHELL:

rebuildall: clean all

all: pdfreport pdfslides

ready:
	mkdir report
	for f in $(wildcard ~/Library/Templates/Markdown/report/*); do \
		ln -s $${f} report/; \
	done
	mkdir slides
	for f in $(wildcard ~/Library/Templates/Markdown/slides/*); do \
		ln -s $${f} slides/; \
	done

# The following does not work.  It seems variables can not occur in wildcard pattern.
#
#	for d in ${TDS}; do \
#		mkdir $${d} ; \
#		for f in $(wildcard ~/Library/Templates/Markdown/"$${d}"/*); do \
#			ln -s $${f} $${d}/; \
#		done ; \
#	done

markdown:
	$(eval OUF = ${OFN}.md)
	${JNC} --to markdown --output=${OUF} ${INF}

pdfreport: markdown
	$(eval OUF = ${OFN}.pdf) \
	${MDC} --latex-engine=${TEX} \
	       --include-in-header=report/preamble.tex \
	       ${PRO} \
	       --bibliography=report/references.bib \
	       --csl=$(CSL) \
	       --output=report/${OUF} \
	       ${IFN}.md

pdfslides: markdown
	$(eval OUF = ${OFN}.pdf) \
	${MDC} --latex-engine=xelatex \
	       --include-in-header=slides/prelude.tex \
	       --include-in-header=slides/preamble.tex \
	       --to=beamer \
	       --variable=theme:default \
	       --variable=colortheme:beetle \
	       --variable=fontsize:14pt \
	       --slide-level=2 \
	       $(PSO) \
	       --output=slides/${OUF} \
	       ${IFN}.md

slideshow:
	${JNC} --to slides --post serve ${INF}

clean:
	rm -f report/*.pdf slides/*.pdf
