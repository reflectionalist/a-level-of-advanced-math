% I Want to Hear Your Voice
% July 22, 2016

1.  What grade are you in?

    \vspace{5mm}

2.  How do you feel about this trial course?

    \vspace{5mm}

    ```
    ----'----'----'----'----'---> interesting
       -2   -1    0    +1   +2

    ----'----'----'----'----'---> systematic
       -2   -1    0    +1   +2

    ----'----'----'----'----'---> intuitive
       -2   -1    0    +1   +2

    ----'----'----'----'----'---> prepared
       -2   -1    0    +1   +2

    ----'----'----'----'----'---> helpful
       -2   -1    0    +1   +2

    ----'----'----'----'----'---> useful
       -2   -1    0    +1   +2

    ----'----'----'----'----'---> clear
       -2   -1    0    +1   +2

    ----'----'----'----'----'---> easy
       -2   -1    0    +1   +2
    ```

    \vspace{5mm}

3.  What do you like about the course?  Why?

    \vfill

4.  What do you dislike about the course?  Why?

    \vfill

5.  What are your ideas to make the course better?

    \vfill

6.  Will you carry on your study after this trial course?

    \vfill

\centering{(: \emph{Thank you!} :)}
